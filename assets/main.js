(function($) {

    /*
     * API FUNCTIONS
     */

    // load data from the API
    function loadData() {
        showLoader(); //show loading indicator
        $.ajax({
                method: 'GET',
                url: 'api/repos',
                data: {},
                dataType: 'json'
            })
            .done(function(data, textStatus, jqXHR) {
                var target = document.getElementById('results-container');
                render(target, data);
                registerModalTriggerEvent();
            })
            .fail(ajaxFail)
            .always(function() {
                hideLoader(); //hide loader after the async call
            });
    }

    //helper function to display a basic alert on error
    function ajaxFail(jqXHR, textStatus, errorThrown) {
        var response = jqXHR.responseJSON;
        alert(response.message);
    }

    // show loading indicator
    function showLoader() {
        $("#loader-container").addClass('active');
    }

    // hide loading indicator
    function hideLoader() {
        $("#loader-container").removeClass('active');
    }

    // send a refresh call
    function refreshData() {
        showLoader(); //show loading indicator

        $.ajax({
                method: 'POST',
                url: 'api/repos/refresh',
            })
            .done(function(data, textStatus, jqXHR) {
                loadData();
            })
            .fail(ajaxFail)
            .always(function() {
                hideLoader(); //hide loader after the async call
            });
    }

    /*
     * TEMPLATING FUNCTIONS
     */

    // helper function to display relative and formatted time
    Handlebars.registerHelper('formatTime', function(time) {
        var relativeTime = moment(time).fromNow(); //relative time
        var formattedTime = moment(time).format('MMMM Do YYYY, h:mm:ss a'); //formatted time

        return new Handlebars.SafeString(
            "<span>" + relativeTime + " (" + formattedTime + ")</span>"
        );
    });

    function render(target, context) {
        var source = document.getElementById("entry-template").innerHTML;
        var template = Handlebars.compile(source);
        var html = template(context);
        target.innerHTML = html;
    }

    /*
     * REGISTER EVENTS
     */

    //open detail modal on click
    function registerModalTriggerEvent() {
        $('.modal-trigger').on('click', function(e) {
            e.preventDefault();
            var $this = $(this);
            var modalID = $this.data('modalid');
            $('#modal-' + modalID).modal('show');
        });
    }

    // on document ready
    $(function() {

        loadData();

        // register on click event
        $('#refreshButton').on('click', function(e) {
            e.preventDefault();
            refreshData();
        });

        /**
         * TABLE SORTING
         */
        $('#repos-table').tablesort(); //make the table sortable

        //helper function to sort columns with numbers
        $('thead th.number').data('sortBy', function(th, td, tablesort) {
            return parseInt(td.text(), 10);
        });

    });
}(jQuery));