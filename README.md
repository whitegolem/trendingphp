# Trending GitHub PHP repositories

App with a a simple API that lists the most starred PHP repositories on GitHub.

A Demo of the project is visible [here](https://3bees.it/trendingphp/)

## Architecture

### Back-end
The API exposes 2 endpoints:

1. POST /repos/refresh - *Connects to the github API and stores the data into the database*
2. GET /repos - *Returns a list of the most starred PHP repositories on Github*

All API calls are dispatched by the router (`router.php`) to the controllers.
The data is persisted to the database with no modification, but is escaped before being presented by the model (with the `sanitize` function in `Models/Repo.php`).

### Front-end
On page loaded, the client sends a `list` AJAX call to the backend to get a list of the repos.
Clicking on the `update` button, a `refresh` call is sent to the backend followed by a `list` call.

The `view` button in every column of the table shows the details of the corresponding repo.
Clicking on the headers `name` or `stars` the results are sorted accordingly (click multiple times to toggle ascending/descending).

## Development
In development I've used `scotchbox`, a LAMP/LEMP Stack vagrant box : [more on scotchbox](https://box.scotch.io/)


## Installation

### Clone the repo

```bash
$ cd ~/webroot
$ git clone https://whitegolem@bitbucket.org/whitegolem/trendingphp.git
```

### Install dependencies
Install dependencies with `Composer`.

```bash
$ composer install
```

Read more about how to install and use `Composer` on your local machine [here](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx).

**Note that all the API calls are redirected to the router by the `.htaccess` file; ensure that your webserver permits the use of .htaccess files.**

### Create a database
Create an empty database; the embedded ORM will create the table and the structure automatically.

### Edit the configuration file

1. Copy `app/config.sample.php` to `app/config.php`.
2. Edit the file `app/config.php` with your database connection parameters. (remember to create an empty database!)
3. Change the `base_URL` param if you change the directory name of the project.

When `production` is set to `false` the db structure is created automatically by the embedded ORM (RedBean).

## Frameworks (backend)

### FastRoute

This library provides a fast implementation of a regular expression based router
[FastRoute](https://github.com/nikic/FastRoute)

### Guzzle

Guzzle, an extensible PHP HTTP client
[Guzzle](https://github.com/guzzle/guzzle)

### RedBean

RedBeanPHP is an easy to use ORM tool for PHP that automatically creates tables and columns as you go
[RedBeanPHP](https://github.com/gabordemooij/redbean)

## Frameworks (frontend)

### jQuery

Javascript library for document traversal and manipulation, event handling, animation, Ajax...
[jQuery](https://jquery.com/)

### Handlebars.js

Templating engine
[jQuery](https://github.com/wycats/handlebars.js/)

### Moment.js

Parse, validate, manipulate, and display dates and times in JavaScript.
[Moment.js](https://github.com/moment/moment/)

### Semantic-UI

Semantic is a UI framework designed for theming
[Semantic-UI](https://github.com/Semantic-Org/Semantic-UI/)

## License

Licensed under the MIT license.
