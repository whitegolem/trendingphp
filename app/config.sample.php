<?php
/**
 * copy this file to "config.php" and fill in the values.
 */
return array(
	'production' => false, // in production redbean (the ORM) cannot modify the db structure
	'base_URL' => '/trendingphp', //no need to change this unless you change the directory name of the project
	// db params:
    'mysql_connection' => array(
        'dbname' => 'trendingphp',
        'user' => 'root',
        'password' => 'root',
        'host' => 'localhost',
    ),
);
 ?>