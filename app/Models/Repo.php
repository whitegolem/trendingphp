<?php namespace App\Models;

use \RedBeanPHP\R as R;

class Repo extends \RedBeanPHP\SimpleModel {

	/**
	 * fields containing the data coming from github
	 */
	private $fieldsToSanitize = array(
		"repoid",
		"name",
		"url",
		"description",
		"stars",
	);

    /**
     * Escape Data for presentation
     */
    private function sanitize()
    {
       	foreach ($this->fieldsToSanitize as $field) {
       		$this->$field = htmlspecialchars($this->$field);
    	}
    }
    
    public function open() {
       // echo "\n\rcalled open: ".$this->id;
        // echo "\n\rcalled open() ".$this->bean;
       $this->sanitize();
    }
}