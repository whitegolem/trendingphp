<?php
require_once __DIR__.'/bootstrap.php';

/**
 * ROUTER
 * the configuration file has been loaded in the bootstrap file
 */

/**
 * create a dispatcher and register the routes
 * each route is managed by a controller and one of it's functions.
 */
$API_base_URL = $config['base_URL'].'/api';
$dispatcher = FastRoute\simpleDispatcher(function(FastRoute\RouteCollector $r) use ($API_base_URL) {
    $r->addRoute('GET', "${API_base_URL}/repos", 'App\Controllers\RepoController/listItems');
    $r->addRoute('POST', "${API_base_URL}/repos/refresh", 'App\Controllers\RepoController/refresh');
});

// get current fetch method and URI
$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];

// Strip query string (?foo=bar) and decode URI
if (false !== $pos = strpos($uri, '?')) {
    $uri = substr($uri, 0, $pos);
}
$uri = rawurldecode($uri);

// dispatch the current route
$routeInfo = $dispatcher->dispatch($httpMethod, $uri);

// create a BaseController to manage common routes or errors
$baseController = new \App\Controllers\BaseController();

switch ($routeInfo[0]) {
    case FastRoute\Dispatcher::NOT_FOUND:
        // ... 404 Not Found
        $baseController->notFound();
        break;
    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $allowedMethods = $routeInfo[1];
        // ... 405 Method Not Allowed
        $baseController->notAllowed();
        break;
    case FastRoute\Dispatcher::FOUND:
        $handler = $routeInfo[1];
        $vars = $routeInfo[2];
        // ... call $handler with $vars
        list($class, $method) = explode("/", $handler, 2);
    	call_user_func_array(array(new $class, $method), $vars);
        break;
}