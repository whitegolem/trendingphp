<?php

// load configuration file or die
try {
    $config = require __DIR__.'/config.php';
} catch(Exception $e) {
    die('error, a config file must be specified');
}

require_once __DIR__.'/../vendor/autoload.php';

/**
 * ORM
 */
define( 'REDBEAN_MODEL_PREFIX', 'App\\Models\\' ); // used by redbean to connect to the model class

use \RedBeanPHP\R as R;
$db_params = $config['mysql_connection'];
R::setup( "mysql:host=${db_params['host']};dbname=${db_params['dbname']}", $db_params['user'], $db_params['password'] );

//do not alter the tables structure in production
if($config['production'])
	R::freeze( TRUE );