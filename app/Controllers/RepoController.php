<?php namespace App\Controllers;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7; //exception hadndling
use GuzzleHttp\Exception\RequestException; //exception hadndling
use \RedBeanPHP\R as R;

class RepoController extends BaseController
{
	/**
	 * update data from github, save it to the database and notify the client
	 */
	public function refresh()
	{
		$this->updateData();
		$response = array(
			"success" => true,
			"message" => "database updated",
		);
		header('HTTP/1.1 201 Created');
		$this->printJSON($response);
	}

	/*
	 * list the repos
	 */
	public function listItems()
	{
		try {
			$repos = R::findAll('repo');

			//if there are no repos then update automatically
			if(empty($repos))
			{
				$this->updateData();
				$repos = R::findAll('repo');
			}

			$response = array(
				"results" => R::exportAll( $repos ),
				"total" => count($repos),
			);
			$this->printJSON($response);
		} catch (\Exception $e) {
			$response = array(
				"error" => true,
				"message" => $e->getMessage(),
			);
			header('HTTP/1.1 500 Internal Server Error');
			$this->printJSON($response);
		}
	}

	/**
	 * update data on the database
	 */
	private function updateData()
	{
		$this->wipe(); //empty the repos table

		//get the data
		$data = $this->getRemoteData();
		$items = $data->items;

		//persist to database
		foreach ($items as $item) {
			$this->save($item);
		}
	}

	/**
	 * get remote data
	 */
	private function getRemoteData()
	{
		//create the HTTP client (guzzle)
		$client = new Client([
		    // Base URI is used with relative requests
		    'base_uri' => 'https://api.github.com/',
		]);

		/**
		 * networking error may occour (connection timeout, DNS errors, etc...)
		 */
		try {
			$remote_response = $client->request('GET', '/search/repositories', [
			    'headers'        => ['Accept' => 'application/vnd.github.v3+json'],
			    'decode_content' => false,
			    'query' => [
			    	'q' => 'language:php',
			    	'sort' => 'stars',
			    	'desc' => 'desc'
			    ]
			]);
		} catch (RequestException $e) {
		    $request =  Psr7\str($e->getRequest());
		    if ($e->hasResponse()) {
		        $message = Psr7\str($e->getResponse());
		        $response = array(
					"error" => true,
					"message" => $request." - ".$message,
				);
				$this->printJSON($response);
		    }
		}

		// Check if a header exists.
		if ($remote_response->hasHeader('Content-Length')) {
			$stream = $remote_response->getBody();
			$contents = $stream->getContents(); // returns all the contents

			$decoded_results = json_decode($contents);
			//everything is ok; return data
			return $decoded_results;
		}else {
			$response = array(
				"error" => true,
				"message" => "no content",
			);
			header('HTTP/1.1 500 Internal Server Error');
			$this->printJSON($response);
		}
	}

	/**
	 * persist data
	 */
	private function save($item)
	{
		$repo = R::dispense( 'repo' );
		$repo->repoid = $item->id;
		$repo->name = $item->name;
		$repo->url = $item->html_url;
		$repo->description = $item->description;
		$repo->stars = $item->stargazers_count;
		$repo->created_at = $item->created_at;
		$repo->pushed_at = $item->pushed_at;
		try{
			$id = R::store( $repo );
		} catch (\Exception $e) {
			$response = array(
				"error" => true,
				"message" => "problems updating the database. ".$e->getMessage(),
			);
			header('HTTP/1.1 500 Internal Server Error');
			$this->printJSON($response);
		}
	}

	/**
	 * delete all data from the table
	 */
	private function wipe()
	{
		R::wipe( 'repo' );
	}

}